import torch


class Quantizer:
    r"""Quantizer for quantizing input feature maps.

    Args:
        dims (tuple of ints): The desired dimensions to quantize on

    Example:
        >>> q = Quantizer(0, 2)
        >>> x = torch.randn(2, 3, 5, 7)
        >>> q.add_sample(x)
        >>> q.calib()
        >>> x_q = q(x)
        >>> x_dq = q.unquantize(x_q)
        >>> x_dq - x_q
        torch.Size([2, 5])
    """

    def __init__(self, levels: int, dims: [int]):
        if dims is None or len(dims) <= 0:
            dims = [0]
        self.dims: list[int] = sorted(list(set(dims)))
        self.levels = levels
        self.samples = None
        self.qmap = None

    def validate(self, x: torch.Tensor):
        assert len(x.shape) >= max(self.dims), ValueError(f"too less dimensions! should len(x.shape)>={max(self.dims)}")
        tail = sorted(list(set(range(len(x.shape))) - set(self.dims)))
        return x, tail

    def x_reshape(self, x: torch.Tensor):
        x, tail = self.validate(x)
        return x.permute(*self.dims, *tail).reshape(*[x.shape[d] for d in self.dims], -1)

    def add_sample(self, x: torch.Tensor):
        """
        add_sample(x: torch.Tensor) -> None

        Add a sample into the Quantizer.
        Samples will be reshaped according to the `dims` specified at init
        and concat with the previous samples for analysis.
        """
        sample = self.x_reshape(x)
        if self.samples is None:
            self.samples = sample
        else:
            self.samples = torch.cat((self.samples, sample), len(self.dims))

    def calib(self):
        """
        calib() -> None

        Calculate the parameters for quantization(i.e. quantization threshold)
        according to the samples added by `add_sample`.
        """
        samples, _ = torch.sort(self.samples, dim=len(self.dims))
        self.samples = None
        idx = torch.arange(0, samples.shape[-1], samples.shape[-1] / self.levels)
        idx_f, idx_c = torch.floor(idx).type(torch.long), torch.ceil(idx).type(torch.long)

        num_dims = len(samples.shape)
        samples = samples.permute(num_dims - 1, *(list(range(num_dims - 1))))
        self.qmap = ((samples[idx_f, ...] + samples[idx_c, ...]) / 2).permute(*(list(range(1, num_dims))), 0)

    def x_unreshape(self, x: torch.Tensor, shape: [int]):
        tail = sorted(list(set(range(len(shape))) - set(self.dims)))
        x = x.reshape(*[shape[t] for t in self.dims], *[shape[t] for t in tail])
        permution = {d: i for i, d in list(enumerate(self.dims + tail))}
        permution = [permution[i] for i in permution.keys()]
        return x.permute(*permution)

    def __call__(self, x: torch.Tensor):
        x, tail = self.validate(x)
        x = self.x_reshape(x)
        pass


if __name__ == "__main__":
    q = Quantizer(10, (0, 2))
    q.add_sample(torch.randn(2, 3, 5, 7))
    q.add_sample(torch.randn(2, 7, 5, 4))
    q.calib()
    print(q)
